Supports both [Firebase Legacy](https://firebase.google.com/docs/cloud-messaging/http-server-ref) and [Firebase v1 Messages](https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages)

### Overview
1. Create a **Client**
2. Create **Request** object for corresponding request
3. Create execute legacy request on legacy client and v1 request on v1 client

### Namespaces
* V1 classes are namespaced to **Teraception\Firebase\Messaging\V1**
* Legacy classes are namespaced to **Teraception\Firebase\Messaging\Legacy**