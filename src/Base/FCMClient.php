<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:22 PM
 */

namespace Teraception\Firebase\Messaging\Base;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Exception\GuzzleException;
use Teraception\Firebase\Messaging\Base\Requests\IRequest;
use Teraception\Firebase\Messaging\Base\Responses\Response;

abstract class FCMClient implements IFCMClient
{
    protected $client;

    /**
     * @param array|null $guzzleConfig
     */
    protected function __construct($guzzleConfig = null)
    {
        $config = $guzzleConfig;
        if($config===null)
            $config = [];

        $this->client = new Client($config);
    }

    /**
     * @param IRequest $request
     * @param \Closure $onResponse (Teraception\Firebase\Messaging\Base\Responses $response)=>boolean
     * @return Response[]
     */
    function executeRequest(IRequest $request, \Closure $onResponse = null)
    {
        $responses = [];
        $continue = true;
        $request->init();
        while($request->needToRequest() && $continue) {
            $resp = null;
            $request->buildNextRequestOptions();
            $options = $request->getRequestOptions();
            $this->setCommonOptions($options);
            try {
                $resp = $this->client->request($request->getMethod(), $request->getUri(), $options);
                $resp = $request->extractResponse($resp,$options,true);
            } catch (GuzzleException $e) {
                $resp = $request->extractResponse($e, $options,false);
            }

            if($onResponse!==null) {
                $continue = $onResponse($resp);
            }
            $responses[] = $resp;
        }

        return $responses;
    }

    protected function setCommonOptions(&$options) {
        if(!isset($options['headers'])){
            $options['headers'] = [];
        }
    }

    /**
     * @param IRequest $request
     * @param integer $concurrency
     * @return Promise {Promise<Response[]>}
     */
    function executeAsyncOnPool(IRequest $request, $concurrency = 5)
    {
        $optionsArr = [];
        $responses = [];
        $request->init();
        $requests = function (IRequest $request, &$optionsArr) {
            while($request->needToRequest()) {
                yield function () use ($request, &$optionsArr) {
                    $request->buildNextRequestOptions();
                    $options = $request->getRequestOptions();
                    $this->setCommonOptions($options);
                    $optionsArr[] = $options;
                    return $this->client->requestAsync($request->getMethod(), $request->getUri(), $options);
                };
            }
        };

        $pool = new Pool($this->client, $requests($request, $optionsArr), [
            'concurrency' => $concurrency,
            'fulfilled' => function ($response, $index) use ($request, &$optionsArr, &$responses) {
                $options = $optionsArr[$index];
                unset($optionsArr[$index]);
                $responses[] = $request->extractResponse($response, $options,true);
            },
            'rejected' => function ($reason, $index) use ($request, &$optionsArr, &$responses) {
                /**
                 * @var RequestException $reason
                 */
                $options = $optionsArr[$index];
                unset($optionsArr[$index]);
                $responses[] = $request->extractResponse($reason, $options,false);
            }
        ]);

        $p = new Promise(function() use(&$responses, $pool, &$p) {
            $p->resolve($pool->promise()->then(function() use (&$responses) {
                return $responses;
            }));
        });
        return $p;
    }
}