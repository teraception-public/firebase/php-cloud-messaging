<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:22 PM
 */

namespace Teraception\Firebase\Messaging\Base;

use Teraception\Firebase\Messaging\Base\Requests\IRequest;

interface IFCMClient
{
    function executeRequest(IRequest $request, \Closure $onResponse = null);
    function executeAsyncOnPool(IRequest $request, $concurrency = 5);
}