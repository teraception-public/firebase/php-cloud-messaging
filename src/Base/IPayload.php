<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 10:33 PM
 */

namespace Teraception\Firebase\Messaging\Base;


interface IPayload
{
    function setData($data);
    function getData();
    function setNotification($notification);
    function getNotification();
    function build();
}