<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:57 PM
 */

namespace Teraception\Firebase\Messaging\Base\Requests;


use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Teraception\Firebase\Messaging\Base\Responses\IResponse;
use Teraception\Firebase\Messaging\Base\Responses\Response;

abstract class BaseRequest implements IRequest, \JsonSerializable
{
    /**
     * @param ResponseInterface|RequestException|GuzzleException $response
     * @param array $requestOptions
     * @param boolean $isSuccess
     * @return IResponse
     */
    function extractResponse($response, $requestOptions, $isSuccess)
    {
        $resp = new Response();
        $resp->setStatus($isSuccess);
        if($resp->getStatus()) {
            $resp->setData(json_decode($response->getBody()->getContents(),true));
        } else {
            $resp->setData($response->getMessage());
        }

        $resp->setRequestOptions($requestOptions);
        return $resp;
    }

    function jsonSerialize()
    {
        return $this->getRequestOptions();
    }

}