<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:57 PM
 */

namespace Teraception\Firebase\Messaging\Base\Requests;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Teraception\Firebase\Messaging\Base\Responses\IResponse;

interface IRequest
{
    function getMethod();
    function init();
    function getUri();

    /**
     * @param ResponseInterface|RequestException|GuzzleException $response
     * @param RequestInterface $request
     * @param boolean $isSuccess
     * @return IResponse
     */
    function extractResponse($response, $request, $isSuccess);
    function needToRequest();
    function buildNextRequestOptions();
    function getRequestOptions();
}