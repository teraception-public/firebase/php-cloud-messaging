<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Base\Requests;

use Teraception\Firebase\Messaging\Base\IPayload;

abstract class SendRequest extends BaseRequest
{
    protected $message;
    /**
     * @var string
     */
    public $topic;
    protected $tokens;
    /**
     * @var string
     */
    public $condition;
    protected $currentStage;
    protected $options;

    public function __construct(IPayload $message)
    {
        $this->message = $message;
    }

    public function setTokens($tokens) {
        $this->tokens = array_chunk($tokens,1000);
    }

    function getMethod()
    {
        return 'post';
    }

    function needToRequest()
    {
        foreach ($this->currentStage as $key => $value) {
            if(!empty($value))
                return true;
        }

        return false;
    }

    abstract protected function utilizeTopic(&$body);
    abstract protected function utilizeTokens(&$body);

    protected function utilizeCondition(&$body) {
        if(isset($this->currentStage['condition'])) {
            $body['condition'] = $this->currentStage['condition'];
            unset($this->currentStage['condition']);
        }
    }

    protected function getBuiltMessage() {
        return $this->message->build();
    }

    function getRequestOptions()
    {
        return $this->options;
    }

    protected function updateBuiltMessage($message){
        $this->utilizeCondition($message);
        $this->utilizeTokens($message);
        $this->utilizeTopic($message);
        return $message;
    }

    function buildNextRequestOptions()
    {
        $this->options = [
            'headers'=>[
                'Content-Type'=>'application/json'
            ],
            'json'=>$this->updateBuiltMessage($this->getBuiltMessage())
        ];
    }

    function init()
    {
        $this->currentStage = [
            'topic'=>$this->topic,
            'tokens'=>$this->tokens,
            'condition'=>$this->condition
        ];
    }
}