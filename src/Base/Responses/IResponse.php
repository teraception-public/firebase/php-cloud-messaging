<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/12/2018
 * Time: 12:11 AM
 */

namespace Teraception\Firebase\Messaging\Base\Responses;

interface IResponse
{
    function getStatus();
    function setStatus($status);

    /**
     * Request options used for this response
     * @return array
     */
    function getRequestOptions();

    /**
     * @param array $requestOptions
     * @return void
     */
    function setRequestOptions($requestOptions);

    /**
     * @return mixed|string
     */
    function getData();
    function setData($data);
}