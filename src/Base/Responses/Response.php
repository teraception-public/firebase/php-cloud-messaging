<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/12/2018
 * Time: 12:11 AM
 */

namespace Teraception\Firebase\Messaging\Base\Responses;


class Response implements IResponse,\JsonSerializable
{
    protected $status;
    protected $data;
    /**
     * @var array $requestOptions
     */
    protected $requestOptions;

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function getData()
    {
        return $this->data;
    }

    function setData($data)
    {
        $this->data = $data;
    }
	
	function jsonSerialize() {
		return [
			'status'=>$this->status,
			'data'=>$this->data,
            'request'=>$this->requestOptions
		];
	}

    /**
     * @return array
     */
    function getRequestOptions()
    {
        return $this->requestOptions;
    }

    /**
     * @param array $requestOptions
     * @return void
     */
    function setRequestOptions($requestOptions)
    {
        $this->requestOptions = $requestOptions;
    }
}