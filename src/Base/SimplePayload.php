<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 10:33 PM
 */

namespace Teraception\Firebase\Messaging\Base;


class SimplePayload implements IPayload, \JsonSerializable
{
    protected $payload;

    /**
     * @param array $payload
     */
    public function __construct($payload = [])
    {
        $this->payload = $payload;
    }

    function setData($data)
    {
        $this->payload['data'] = $data;
    }

    function getData()
    {
        return $this->payload['data'];
    }

    function setNotification($notification)
    {
        $this->payload['notification'] = $notification;
    }

    function getNotification()
    {
        return $this->payload['notification'];
    }

    function build()
    {
        return $this->payload;
    }

    public function jsonSerialize()
    {
        return $this->payload;
    }
}