<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:22 PM
 */

namespace Teraception\Firebase\Messaging\Legacy;


use Teraception\Firebase\Messaging\Base\Requests\IRequest;
use Teraception\Firebase\Messaging\Base\Responses\Response;
use Teraception\Firebase\Messaging\Legacy\Requests\ILegacyRequest;

class FCMClient extends \Teraception\Firebase\Messaging\Base\FCMClient
{
    protected $serverKey;
    protected $senderId;

    /**
     * LegacyFCMClient constructor.
     * @param string $serverKey
     * @param string $senderId
     */
    public function __construct($serverKey,$senderId)
    {
        $config = [
            'base_uri'=>'https://fcm.googleapis.com/fcm/'
        ];
        parent::__construct($config);
        $this->senderId = $senderId;
        $this->serverKey = $serverKey;
    }

    protected function getAuthKey()
    {
        return $this->serverKey;
    }

    protected function setCommonOptions(&$options)
    {
        parent::setCommonOptions($options);
        $options['headers']['Authorization'] = "key=".$this->getAuthKey();
        $options['headers']['project_id'] = $this->senderId;
    }

    /**
     * @param ILegacyRequest $request
     * @param int $concurrency
     * @return \GuzzleHttp\Promise\Promise
     */
    function executeAsyncOnPool(IRequest $request, $concurrency = 5)
    {
        return parent::executeAsyncOnPool($request, $concurrency);
    }

    /**
     * @param ILegacyRequest $request
     * @param \Closure $onResponse (Teraception\Firebase\Messaging\Base\Responses $response)=>boolean
     * @return \Teraception\Firebase\Messaging\Base\Responses\Response[]
     */
    public function executeRequest(IRequest $request, \Closure $onResponse = null)
    {
        return parent::executeRequest($request, $onResponse);
    }
}