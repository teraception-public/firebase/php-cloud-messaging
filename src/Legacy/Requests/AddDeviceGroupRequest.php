<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Legacy\Requests;

use Teraception\Firebase\Messaging\Base\Requests\BaseRequest;

class AddDeviceGroupRequest extends BaseRequest implements ILegacyRequest
{
    protected $sent;
    protected $keyName;
    protected $groupKey;
    protected $ids;
    protected $options;

    /**
     * AddDeviceGroupRequest constructor.
     * @param string $group_key existing device group key
     * @param array $ids device tokens
     * @param null|string $keyName  device group key name
     */
    public function __construct($group_key, array $ids,$keyName = null)
    {
        $this->groupKey = $group_key;
        $this->sent = false;
        $this->keyName = $keyName;
        $this->ids = $ids;
    }

    function getMethod()
    {
        return 'POST';
    }

    function init()
    {
        $this->sent = false;
    }

    function getUri()
    {
        return 'notification';
    }

    function needToRequest()
    {
        return !$this->sent;
    }

    function buildNextRequestOptions()
    {
        $d = [
            'operation'=>'add',
            'notification_key'=>$this->groupKey,
            'registration_ids'=>$this->ids
        ];
        if(!empty($this->keyName)){
            $d['notification_key_name']=$this->keyName;
        }

        $opts = [
            'headers'=>['Content-Type'=>'application/json'],
            'json'=>$d
        ];
        $this->sent = true;
        $this->options = $opts;
    }

    function getRequestOptions()
    {
        return $this->options;
    }
}