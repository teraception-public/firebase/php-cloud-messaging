<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */


namespace Teraception\Firebase\Messaging\Legacy\Requests;

use Teraception\Firebase\Messaging\Base\Requests\BaseRequest;

class CreateDeviceGroupRequest extends BaseRequest implements ILegacyRequest
{
    protected $sent;
    protected $keyName;
    protected $ids;
    protected $options;

    /**
     * CreateDeviceGroupRequest constructor.
     * @param $keyName string device group key name
     * @param array $ids device tokens
     */
    public function __construct($keyName,array $ids)
    {
        $this->sent = false;
        $this->keyName = $keyName;
        $this->ids = $ids;
    }

    function getMethod()
    {
        return 'POST';
    }

    function init()
    {
        $this->sent = false;
    }

    function getUri()
    {
        return 'notification';
    }

    function needToRequest()
    {
        return !$this->sent;
    }

    function buildNextRequestOptions()
    {
        $this->options = [
            'headers'=>['Content-Type'=>'application/json'],
            'json'=>[
                'operation'=>'create',
                'notification_key_name'=>$this->keyName,
                'registration_ids'=>$this->ids
            ]
        ];

        $this->sent = true;
    }

    function getRequestOptions()
    {
        return $this->options;
    }
}