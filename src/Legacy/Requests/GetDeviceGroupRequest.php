<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Legacy\Requests;

use Teraception\Firebase\Messaging\Base\Requests\BaseRequest;

class GetDeviceGroupRequest extends BaseRequest implements ILegacyRequest
{
    protected $sent;
    protected $keyName;
    protected $groupKey;
    protected $options;
    protected $ids;

    /**
     * GetDeviceGroupRequest constructor.
     * @param string $keyName existing device group key name
     */
    public function __construct($keyName = null)
    {
        $this->sent = false;
        $this->keyName = $keyName;
    }

    function getMethod()
    {
        return 'GET';
    }

    function init()
    {
        $this->sent = false;
    }

    function getUri()
    {
        return 'notification';
    }

    function needToRequest()
    {
        return !$this->sent;
    }

    function getRequestOptions()
    {
        return $this->options;
    }

    function buildNextRequestOptions()
    {
        $this->options = [
            'headers'=>['Content-Type'=>'application/json'],
            'query'=>[
                'notification_key_name'=>$this->keyName
            ]
        ];
        $this->sent = true;
    }
}