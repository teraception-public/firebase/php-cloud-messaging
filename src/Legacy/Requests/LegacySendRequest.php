<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Legacy\Requests;

use Teraception\Firebase\Messaging\Base\Requests\SendRequest;
use Teraception\Firebase\Messaging\Legacy\SendPayload;

class LegacySendRequest extends SendRequest implements ILegacyRequest
{
    public $group;

    public function __construct(SendPayload $message)
    {
        parent::__construct($message);
    }

    function init()
    {
        parent::init();
        $this->currentStage['group'] = $this->group;
    }

    public function updateBuiltMessage($message)
    {
        $body = parent::updateBuiltMessage($message);
        if(isset($this->currentStage['group'])) {
            $body['to'] = $this->currentStage['group'];
            unset($this->currentStage['group']);
        }
        return $body;
    }

    protected function utilizeTopic(&$body) {
        if(isset($this->currentStage['topic'])) {
            $body['to'] = '/topics/'.$this->currentStage['topic'];
            unset($this->currentStage['topic']);
        }
    }

    protected function utilizeTokens(&$body){
        if(isset($this->currentStage['tokens'])) {
            $body['registration_ids'] = $this->currentStage['tokens'][0];
            unset($this->currentStage['tokens'][0]);
            $this->currentStage['tokens'] = array_values($this->currentStage['tokens']);
        }
    }

    function getUri()
    {
        return 'send';
    }
}