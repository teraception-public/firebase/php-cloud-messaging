<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Legacy\Requests;

use Teraception\Firebase\Messaging\Base\Requests\BaseRequest;

class RemoveDeviceGroupRequest extends BaseRequest implements ILegacyRequest
{
    protected $options;
    protected $sent;
    protected $keyName;
    protected $groupKey;
    protected $ids;

    public function __construct($group_key, array $ids,$keyName = null)
    {
        $this->groupKey = $group_key;
        $this->sent = false;
        $this->keyName = $keyName;
        $this->ids = $ids;
    }

    function getMethod()
    {
        return 'POST';
    }

    function init()
    {
        $this->sent = false;
    }

    function getUri()
    {
        return 'notification';
    }

    function needToRequest()
    {
        return !$this->sent;
    }

    function getRequestOptions()
    {
        return $this->options;
    }

    function buildNextRequestOptions()
    {
        $d = [
            'operation'=>'remove',
            'notification_key'=>$this->groupKey,
            'registration_ids'=>$this->ids
        ];
        if(!empty($this->keyName)){
            $d['notification_key_name']=$this->keyName;
        }

        $this->options = [
            'headers'=>['Content-Type'=>'application/json'],
            'json'=>$d
        ];
        $this->sent = true;
    }
}