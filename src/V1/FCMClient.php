<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:22 PM
 */

namespace Teraception\Firebase\Messaging\V1;

use Teraception\Firebase\Messaging\Base\Requests\IRequest;
use Teraception\Firebase\Messaging\V1\Requests\IV1Request;


class FCMClient extends \Teraception\Firebase\Messaging\Base\FCMClient
{
    protected $projectId;

    /**
     * V1FCMClient constructor.
     * @param string $projectId Firebase project id
     * @param string $authFilePath Absolute path to service account private file
     */
    public function __construct($authFilePath, $projectId)
    {
        parent::__construct([
            'base_uri'=>'https://fcm.googleapis.com/v1/projects/'.$projectId.'/'
        ]);
        $this->projectId = $projectId;
        $client = new \Google_Client();
        putenv("GOOGLE_APPLICATION_CREDENTIALS=".$authFilePath);
        $client->useApplicationDefaultCredentials();
        $client->addScope('https://www.googleapis.com/auth/firebase.messaging');
        $this->client = $client->authorize($this->client);
    }

     /**
     * @param IV1Request $request
     * @param int $concurrency
     * @return \GuzzleHttp\Promise\Promise
     */
    function executeAsyncOnPool(IRequest $request, $concurrency = 5)
    {
        return parent::executeAsyncOnPool($request, $concurrency);
    }

    /**
     * @param IV1Request $request
     * @param \Closure $onResponse (Teraception\Firebase\Messaging\Base\Responses $response)=>boolean
     * @return \Teraception\Firebase\Messaging\Base\Responses\Response[]
     */
    public function executeRequest(IRequest $request, \Closure $onResponse = null)
    {
        return parent::executeRequest($request, $onResponse);
    }
}