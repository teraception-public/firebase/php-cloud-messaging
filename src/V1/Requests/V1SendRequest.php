<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\V1\Requests;

use Teraception\Firebase\Messaging\Base\Requests\SendRequest;
use Teraception\Firebase\Messaging\V1\SendPayload;

/**
 * Class V1SendRequest
 * @property SendPayload $message
 * @package Teraception\Firebase\Messaging\V1\Requests
 */
class V1SendRequest extends SendRequest implements IV1Request
{
    protected $projectId;

    public function __construct(SendPayload $payload, $projectId) {
        parent::__construct($payload);
        $this->projectId = $projectId;
    }

    protected function utilizeTopic(&$body) {
        if(isset($this->currentStage['topic'])) {
            $body['topic'] = $this->currentStage['topic'];
            unset($this->currentStage['topic']);
        }
    }

    public function setTokens($tokens) {
        $this->tokens = $tokens;
    }

    protected function utilizeTokens(&$body) {
        if(isset($this->currentStage['tokens'])) {
            $body['token'] = $this->currentStage['tokens'][0];
            unset($this->currentStage['tokens'][0]);
            $this->currentStage['tokens'] = array_values($this->currentStage['tokens']);
        }
    }

    function setWebPayload($payload) {
        $this->message->addToMessagePayload('webpush', $payload);
    }

    function setApplePayload($payload) {
        $this->message->addToMessagePayload('apns', $payload);
    }

    protected function getBuiltMessage()
    {
        return parent::getBuiltMessage()['message'];
    }

    protected function updateBuiltMessage($message)
    {
        $message = parent::updateBuiltMessage($message);
        $b = $this->message->build();
        $b['message'] = $message;
        return $b;
    }

    function setAndroidPayload($payload) {
        $this->message->addToMessagePayload('android', $payload);
    }

    function getUri()
    {
        return 'https://fcm.googleapis.com/v1/projects/'.$this->projectId.'/messages:send';
    }
}